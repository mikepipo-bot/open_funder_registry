# Crossref Funder Registry

The Funder Registry provides a common taxonomy of international funding body names that funding data participants should use to normalize funder names and IDs for deposit with Crossref.

The list should be used to present authors with a pre-populated "Funder Name" option at the time of submission, and can also be used to match funder names extracted from papers.

The list is available to download as an RDF file, and is freely available under a CC0 license waiver.

## Latest Version

The latest version of the registry is `v1.31`. Download the latest release [here](https://gitlab.com/crossref/open_funder_registry/-/tags).

Funder count: 23,411.

23,160 active funders, 251 defunct or replaced.


A .csv file of the most recent version of the Registry can [also be downloaded](https://doi.crossref.org/funderNames?mode=list) Note that there is no version history at this time for the .csv format.
